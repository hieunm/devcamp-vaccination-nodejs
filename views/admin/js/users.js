"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gUserId = 0;
const gBASE_URL = "http://localhost:8000/api/users";
const gHEADERS = { "Content-Type": "application/json;charset=UTF-8" };

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gUSER_COLS = [
  "stt",
  "fullName",
  "phone",
  "status",
  "createdAt",
  "updatedAt",
  "action",
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gUSER_STT_COL = 0;
const gUSER_FULLNAME_COL = 1;
const gUSER_PHONE_CODE_COL = 2;
const gUSER_STATUS_COL = 3;
const gUSER_CREATED_AT_COL = 4;
const gUSER_UPDATED_AT_COL = 5;
const gUSER_ACTION_COL = 6;

// Khai báo DataTable & mapping collumns
var gUserTable = $("#users-table").DataTable({
  columns: [
    { data: gUSER_COLS[gUSER_STT_COL] },
    { data: gUSER_COLS[gUSER_FULLNAME_COL] },
    { data: gUSER_COLS[gUSER_PHONE_CODE_COL] },
    { data: gUSER_COLS[gUSER_STATUS_COL] },
    { data: gUSER_COLS[gUSER_CREATED_AT_COL] },
    { data: gUSER_COLS[gUSER_UPDATED_AT_COL] },
    { data: gUSER_COLS[gUSER_ACTION_COL] },
  ],
  columnDefs: [
    {
      // định nghĩa lại cột STT
      targets: gUSER_STT_COL,
      render: (data, type, row, meta) => {
        return meta.row + 1;
      },
    },
    {
      // định nghĩa lại cột action
      targets: gUSER_ACTION_COL,
      defaultContent: `
      <i class="fas fa-edit text-primary btn update-user"></i>
      <i class="fas fa-trash-alt text-danger btn delete-user"></i>
      `,
      className: "text-center d-flex justify-content-around",
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // tải trang
  onPageLoading();

  // CRUD
  $("#btn-create-user").click(() => {
    $("#create-user-modal").modal("show");
  });
  $("#users-table").on('click', '.update-user', function () {
    onBtnUpdateUserClick(this);
  });
  $("#users-table").on('click', '.delete-user', function () {
    onBtnDeleteUserClick(this);
  });

  // confirm CRUD
  $("#btn-confirm-create-user").click(onBtnConfirmCreateUserClick);
  $("#btn-confirm-update-user").click(function () {
    onBtnConfirmUpdateUserClick(this)
  });
  $("#btn-confirm-delete-user").click(function () {
    onBtnConfirmDeleteUserClick(this)
  });

  // modal
  $("#create-user-modal").on("hidden.bs.modal", resetCreateForm);
  $("#update-user-modal").on("hidden.bs.modal", resetUpdateForm);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  getAllUsers();
}

//CRUD
async function onBtnUpdateUserClick(paramButton) {
  //B1: Thu thập
  gUserId = collectUserId(paramButton);
  //B2: Validate
  //B3: Xử lý
  try {
    const response = await callAPIGetUserById();
    const result = await response.json();
    handleGetUserByIdSuccess(result);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

function onBtnDeleteUserClick(paramButton) {
  gUserId = collectUserId(paramButton);
  $("#delete-user-modal").modal("show");
}

// confirm CRUD
async function onBtnConfirmCreateUserClick() {
  const newUser = {
    fullName: "",
    phone: "",
    status: "",
  };
  //B1: Thu thập
  collectCreateUser(newUser);
  //B2: Validate
  let vCheck = validateUser(newUser);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPICreateUser(newUser);
      const result = await response.json();
      handleCreateUserSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

async function onBtnConfirmUpdateUserClick() {
  const patchUser = {
    fullName: "",
    phone: "",
    status: "",
  };
  //B1: Thu thập
  collectUpdateContact(patchUser);
  //B2: Validate
  let vCheck = validateUser(patchUser);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPIUpdateContact(patchUser);
      const result = await response.json();
      handleUpdateContactSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

async function onBtnConfirmDeleteUserClick() {
  //B1: Thu thập
  //B2: Validate
  //B3: Xử lý
  try {
    const response = await callAPIDeleteContact();
    const result = await response.json();
    handleDeleteContactSuccess(result);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
async function getAllUsers() {
  try {
    const response = await callAPIGetAllUsers();
    const result = await response.json();
    loadDataToTable(result.data);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

//API
async function callAPIGetAllUsers() {
  return await fetch(gBASE_URL, {
    method: "get",
    mode: "cors",
    headers: gHEADERS,
  });
}

async function callAPICreateUser(paramUser) {
  return await fetch(gBASE_URL, {
    method: "post",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramUser),
  });
}

async function callAPIGetUserById() {
  return await fetch(gBASE_URL + '/' + gUserId, {
    method: "get",
    mode: "cors",
    headers: gHEADERS
  });
}

async function callAPIUpdateContact(paramUser) {
  return await fetch(gBASE_URL + '/' + gUserId, {
    method: "put",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramUser),
  });
}

async function callAPIDeleteContact() {
  return await fetch(gBASE_URL + '/' + gUserId, {
    method: "delete",
    mode: "cors",
    headers: gHEADERS
  });
}

//Collect Data
function collectUserId(paramButton) {
  var vTableRow = $(paramButton).closest("tr");
  var vUserRowData = gUserTable.row(vTableRow).data();
  return vUserRowData._id;
}

function collectCreateUser(paramUser) {
  paramUser.fullName = $.trim($("#inp-fullname-create").val());
  paramUser.phone = $.trim($("#inp-phone-create").val());
  paramUser.status = $("#select-status-create").val();
}

function collectUpdateContact(paramUser) {
  paramUser.fullName = $.trim($("#inp-fullname-update").val());
  paramUser.phone = $.trim($("#inp-phone-update").val());
  paramUser.status = $("#select-status-update").val();
}

//validate Data
function validateUser(paramUser) {
  if (!paramUser.fullName) {
    alert("Fullname is invalid");
    return false;
  }
  if (!validatePhone(paramUser.phone)) {
    alert("Number phone is invalid");
    return false;
  }
  return true;
}

const validateEmail = (email) => {
  const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return email.match(emailFormat) ? true : false;
};

const validatePhone = (phone) => {
  const phoneFormat = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
  return phone.match(phoneFormat) ? true : false;
};

//Hàm dùng chung
function loadDataToTable(paramVoucherArr) {
  gUserTable.clear();
  gUserTable.rows.add(paramVoucherArr);
  gUserTable.draw();
}

function handleCreateUserSuccess(paramResult) {
  alert(paramResult.message);
  $("#create-user-modal").modal("hide");
  getAllUsers();
}

function handleGetUserByIdSuccess(paramResult) {
  $("#update-user-modal").modal("show");
  const data = paramResult.data;
  $("#inp-_id-update").val(data._id);
  $("#inp-fullname-update").val(data.fullName);
  $("#inp-phone-update").val(data.phone);
  $("#select-status-update").val(data.status);
}

function handleUpdateContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#update-user-modal").modal("hide");
  getAllUsers();
}

function handleDeleteContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#delete-user-modal").modal("hide");
  getAllUsers();
}

function resetCreateForm() {
  $("#inp-fullname-create").val("");
  $("#inp-phone-create").val("");
  $("#select-status-create").val("Level 0");
}

function resetUpdateForm() {
  $("#inp-_id-update").val("");
  $("#inp-fullname-update").val("");
  $("#inp-phone-update").val("");
  $("#select-status-update").val("Level 0");
}
