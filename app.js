//thư viện
const express = require("express");
const path = require("path");
const cors = require('cors');
const mongoose = require("mongoose");

//import middleware
const logCurrentTimeMiddleWare = require('./app/middlewares/time.middleware');
const logUrlMiddleWare = require('./app/middlewares/url.middleware');

//import router
const UserRouter = require('./app/routes/user.router');
const ContactRouter = require('./app/routes/contact.router');

//variables
const app = express();
const port = 8000;

//middlewares
app.use(express.json());
app.use(express.static("views"));
app.use(cors());
app.use(logCurrentTimeMiddleWare, logUrlMiddleWare);

//định nghĩa router cho trang home
app.get("/", (req, res) => {
  const indexPath = path.join(__dirname, "./views/home.html");
  res.sendFile(indexPath);
});

//định nghĩa router cho trang admin users
app.get("/admin/users", (req, res) => {
  const indexPath = path.join(__dirname, "./views/admin/users.html");
  res.sendFile(indexPath);
});

//định nghĩa router cho trang admin contacts
app.get("/admin/contacts", (req, res) => {
  const indexPath = path.join(__dirname, "./views/admin/contacts.html");
  res.sendFile(indexPath);
});

//áp dụng các router api
app.use('/api/users', UserRouter);
app.use('/api/contacts', ContactRouter);

//Kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccination")
  .then(() => {
    console.log("Successfully connected to MongoDB");
  })
  .catch((err) => {
    console.log(err.message);
  });

//Khởi tạo cổng
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})